import { Controller, Logger, Post, Body, OnModuleInit, Get, Param } from '@nestjs/common';
import { Client, ClientGrpc, ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { Observable } from 'rxjs';
import * as grpc from 'grpc';


interface Empty { }

export interface IGrpcService {
    accumulate(numberArray: INumberArray): Observable<any>;
    greeting(Empty, metadata: grpc.Metadata): Observable<string>;
}



export interface IGrpcSupplierService {
    greeting(data: {}, metadata: grpc.Metadata): Observable<string>;
}


interface INumberArray {
    data: number[];
}

export const mathMicroserviceOptions: ClientOptions = {
    transport: Transport.GRPC,
    options: {
        url: "localhost:3001",
        package: 'accounts',
        protoPath: join(__dirname, '../../../proto/accounts.proto'),
    },
}
export const supplierMicroserviceOptions: ClientOptions = {
    transport: Transport.GRPC,
    options: {
        url: "localhost:5000",
        package: 'suppliers',
        protoPath: join(__dirname, '../../../proto/suppliers.proto'),
    },
}


@Controller()
export class AccountController implements OnModuleInit {
    private logger = new Logger(`${AccountController.name} Gateway`);

    @Client(mathMicroserviceOptions)
    private client: ClientGrpc;

    @Client(supplierMicroserviceOptions)
    private suppliersClient: ClientGrpc;


    private supplierService: IGrpcSupplierService;
    private grpcService: IGrpcService;


    onModuleInit() {
        this.grpcService = this.client.getService<IGrpcService>('AccountController');
        this.supplierService = this.suppliersClient.getService<IGrpcSupplierService>('SuppliersController');
    }

    @Post('add')
    async accumulate(@Body('data') data: number[]) {

        this.logger.log("this is the add in the gateway")
        return this.grpcService.accumulate({ data });
    }

    @Post()
    greeting(@Body('data') data: number[]) {

        const metadata = new grpc.Metadata();

        metadata.add("fuckingLotteryFam", "Fucking won the lottery fam");

        return this.grpcService.greeting({}, metadata)
    }


    @Get()
    greetingSuppliers() {
        const metadata = new grpc.Metadata();
        return this.supplierService.greeting({}, metadata)
    }

}
