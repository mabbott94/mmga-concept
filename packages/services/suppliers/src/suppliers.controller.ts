import { Controller, Logger } from "@nestjs/common";
import { GrpcMethod } from "@nestjs/microservices";
import { SuppliersService } from "./suppliers.service";
import * as grpc from 'grpc';

interface Empty {}

@Controller()
export class SuppliersController {
  private readonly logger = new Logger(SuppliersController.name);

  @GrpcMethod('SuppliersController', 'Greeting')
  greeting(data: Empty, metadata: grpc.Metadata): any {
    return {
      greeting: "Simon"
    }
  }

}