import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { join } from 'path';
import { Logger } from '@nestjs/common';

const logger = new Logger('Main')

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: "localhost:5000",
      package: 'suppliers',
      protoPath: join(__dirname, '../../../proto/suppliers.proto'),
    },
  });

  logger.log("attempting to listen");

  await app.listenAsync();
}
bootstrap();
