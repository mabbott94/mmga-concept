import { Injectable, Logger } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';

interface FindOneResponse {
  id: string;
  name: string;
}

@Injectable()
export class SuppliersService {
  private readonly logger = new Logger(SuppliersService.name)

  constructor() { }

  findAll() {
    this.logger.log(`find all request, woop!`);

    return {
      id: '1',
      name: 'Totally Great Supplier',
    }
  }

  findOne(id: string): FindOneResponse {
    this.logger.log(`find one request, id: ${id}`);
    return {
      id,
      name: 'Totally Great Supplier',
    }
  }
}

