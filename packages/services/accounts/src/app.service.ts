import { Injectable, Logger } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';

@Injectable()
export class AccountsService {
  private readonly logger = new Logger(AccountsService.name)

  constructor() { }


  create(email: string, password: string) {
    this.logger.log(`create request ${email}, ${password}`)
    return {
      jwtToken: "efqdfawdfqdfa",
      email: 'michael@mabbot.dev',
      timestamp: Date.now().toString()
    }
  }

  login(email: string, password: string) {
    this.logger.log(`login request: ${email}, ${password}`);
    return {
      jwtToken: "efqdfawdfqdfa",
      email: 'michael@mabbot.dev',
    }
  }
}

