// package: accounts
// file: accounts.proto

import * as jspb from "google-protobuf";

export class UserAccountCreateRequest extends jspb.Message {
  getEmail(): string;
  setEmail(value: string): void;

  getPassword(): string;
  setPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserAccountCreateRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UserAccountCreateRequest): UserAccountCreateRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserAccountCreateRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserAccountCreateRequest;
  static deserializeBinaryFromReader(message: UserAccountCreateRequest, reader: jspb.BinaryReader): UserAccountCreateRequest;
}

export namespace UserAccountCreateRequest {
  export type AsObject = {
    email: string,
    password: string,
  }
}

export class UserAccountCreateResponse extends jspb.Message {
  getJwttoken(): string;
  setJwttoken(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getTimestamp(): string;
  setTimestamp(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserAccountCreateResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UserAccountCreateResponse): UserAccountCreateResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserAccountCreateResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserAccountCreateResponse;
  static deserializeBinaryFromReader(message: UserAccountCreateResponse, reader: jspb.BinaryReader): UserAccountCreateResponse;
}

export namespace UserAccountCreateResponse {
  export type AsObject = {
    jwttoken: string,
    email: string,
    timestamp: string,
  }
}

export class LoginUserRequest extends jspb.Message {
  getEmail(): string;
  setEmail(value: string): void;

  getPassword(): string;
  setPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoginUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LoginUserRequest): LoginUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoginUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoginUserRequest;
  static deserializeBinaryFromReader(message: LoginUserRequest, reader: jspb.BinaryReader): LoginUserRequest;
}

export namespace LoginUserRequest {
  export type AsObject = {
    email: string,
    password: string,
  }
}

export class LoginUserResponse extends jspb.Message {
  getEmail(): string;
  setEmail(value: string): void;

  getJwttoken(): string;
  setJwttoken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoginUserResponse.AsObject;
  static toObject(includeInstance: boolean, msg: LoginUserResponse): LoginUserResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoginUserResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoginUserResponse;
  static deserializeBinaryFromReader(message: LoginUserResponse, reader: jspb.BinaryReader): LoginUserResponse;
}

export namespace LoginUserResponse {
  export type AsObject = {
    email: string,
    jwttoken: string,
  }
}

