import { Controller, Logger } from '@nestjs/common';
import { GrpcMethod, } from "@nestjs/microservices";
import * as grpc from 'grpc';


export interface IData {
  data: number[];
}

interface ISum {
  sum: number;
}

interface Empty {

}

@Controller()
export class AccountController {
  private readonly logger = new Logger(AccountController.name);


  @GrpcMethod('AccountController', 'Accumulate')
  accumulate(array: IData, metadata: any): ISum {

    this.logger.log(array.data.toString());
    return {
      sum: array.data.reduce((sum, val) => sum + val)
    }
  }

  @GrpcMethod('AccountController', 'Greeting')
  greeting(data: Empty, metadata: grpc.Metadata): any {

    this.logger.log(metadata.get("fuckinglotteryfam"));
    return {
      greeting: "Michael"
    }
  }
}
