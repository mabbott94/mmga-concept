import { Module } from '@nestjs/common';
import { AccountsService } from './app.service';
import { AccountController } from './account.controller';

@Module({
  imports: [],
  controllers: [AccountController],
  providers: [AccountsService],
})
export class AppModule { }
